===
neo
===


Copy data from RT-130 CF card to a zip file


* Free software: GNU General Public License v3 (GPLv3)



Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `passoft/cookiecutter`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`passoft/cookiecutter`: https://git.passcal.nmt.edu/passoft/cookiecutter
