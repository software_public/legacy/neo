#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `neo` package."""

import unittest
import sys

try:
    import neo
except ImportError:
     pass

class TestNeo(unittest.TestCase):
    """Tests for `neo` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_import(self):
        if 'neo' in sys.modules:
            self.assert_(True, "neo loaded")
        else:
            self.fail()

