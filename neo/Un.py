#!/usr/bin/env picpython

#
#   Read Zip and Tar files from neo
#
#   June 2009, Steve Azevedo
#

PROG_VERSION = '2009.162a'

import sys, os, zipfile, tarfile


class readBuffer (object) :
    '''   buf = string buffer
          ptr = next read position
          len = total len of buffer in bytes
    '''
    __slots__ = 'buf', 'ptr', 'length', 'set', 'clear', 'rewind', 'inc'
    
    def __init__ (self) :
        self.clear ()
        
    def set (self, b) :
        self.buf = b
        self.length = len (b)
        self.ptr = 0
        
    def clear (self) :
        self.buf = None
        self.ptr = None
        self.length = None
        
    def rewind (self) :
        self.ptr = 0
        
    def inc (self, n) :
        self.ptr += n
        
class dataFile (object) :
    __slots__ = 'basefile', 'subfiles', 'fh', 'kind', 'next'
    
    def __init__ (self, basefile = None) :
        #   The filename of directory name
        self.basefile = basefile
        #   Subfiles in zip, tar, or raw file
        self.subfiles = []
        #   The open filehandle or None
        self.fh = None
        #   What kind of file is this?
        self.kind = None

    def next (self) :
        f = None
        try :
            f = self.subfiles[0]
        except IndexError :
            self.subfiles = []
            
        try :
            self.subfiles = self.subfiles[1:]
        except IndexError :
            self.subfiles = []

        return f
        
class neoZIP :
    '''   rt-130 zip file reader
          Uses the module zipfile to process the contents
    '''
    def __init__ (self, filename, verbose = False) :
        self.verbose = verbose
        self.df = dataFile (filename)
        self.df.kind = 'ZIP'
        self.buf = readBuffer ()
        
    def open (self) :
        self._get_raw_names ()
    
    def read (self, fileRE) :
        return self._get_raw_file_buf (fileRE)
        
    def close (self) :
        if self.verbose :
            sys.stderr.write ("Closing: %s" % self.df.basefile)
            
        self.df.fh.close ()
        self.df.fh = None
        
    def getPacket (self, fileRE) :
        pbuf = None
        if self.buf.ptr >= self.buf.length :
            self._get_raw_file_buf (fileRE)
            
        ptr = self.buf.ptr
        l = self.buf.length
        
        if ptr < l :
            pbuf = self.buf.buf[ptr:PACKET_SIZE + ptr]
            len_pbuf = len (pbuf)
            self.buf.inc (len_pbuf)
            if len_pbuf != PACKET_SIZE :
                sys.stderr.write ("Read Error: read %d of %d\n" % (len_pbuf, PACKET_SIZE))

        return pbuf
                
    def _get_raw_file_buf (self, fileRE) :
        self.buf.clear ()
        buf = ''
        #   Get next file from sorted list
        z = self.df.next ()
        #print z
        if z :
            #   Only select files that match RE
            #if fileRE.match (z) or sohRE.match (z) :
            if fileRE.match (z) :
                if self.verbose :
                    sys.stderr.write ("\tReading: %s\n" % z)
                    
                try :
                    buf = self.df.fh.read (z)
                except Exception, e :
                    sys.stderr.write ("%s\n" % e)
                    
                #   XXX Should really check the size of the buffer here!
                if buf :
                    self.buf.set (buf)
                    
            return True
        else :
            return False
        
    def _get_raw_names (self) :
        #   Open the zip file
            
        try :
            self.df.fh = zipfile.ZipFile (self.df.basefile)
        except Exception, e :
            sys.stderr.write ("%s\n" % e)
            
        #   Get a list of filenames
        zipnames = self.df.fh.namelist ()
        #   Sort then (by time)
        zipnames.sort ()
        self.df.subfiles = zipnames
        
class neoTAR :
    '''   rt-130 tar file reader
          Uses the module tarfile to process the contents
    '''
    def __init__ (self, filename, verbose = False) :
        self.verbose = verbose
        self.df = dataFile (filename)
        self.df.kind = 'tar'
        self.buf = readBuffer ()
        
    def open (self) :
        self._get_raw_names ()
    
    def read (self, fileRE) :
        return self._get_raw_file_buf (fileRE)
        
    def close (self) :
        if self.verbose :
            sys.stderr.write ("Closing: %s\n" % self.df.basefile)
            
        self.df.fh.close ()
        self.df.fh = None
        
    def getPacket (self) :
        pbuf = None
        if self.buf.ptr >= self.buf.length :
            self._get_raw_file_buf ()
            
        ptr = self.buf.ptr
        l = self.buf.length
        
        if ptr < l :
            pbuf = self.buf.buf[ptr:PACKET_SIZE + ptr]
            len_pbuf = len (pbuf)
            self.buf.inc (len_pbuf)
            if len_pbuf != PACKET_SIZE :
                sys.stderr.write ("Read Error: read %d of %d\n" % (len_pbuf, PACKET_SIZE))

        return pbuf
    
    #   Compare tarfile members by file name
    def _member_cmp (self, a, b) :
        return cmp (a.name, b.name)
        
    def _get_raw_file_buf (self, fileRE) :
        self.buf.clear ()
        #   Get next member
        m = self.df.next ()
        if m :
            if self.verbose :
                sys.stderr.write ("\tReading: %s" % m.name)
                
            #   Does it match our expected file name?
            #if fileRE.match (m.name) or sohRE.match (m.name) :
            if fileRE.match (m.name) :
                try :
                    buf = self.df.fh.extractfile (m).read ()
                except Exception, e :
                    sys.stderr.write ("%s\n" % e)
                    
                if buf :
                    self.buf.set (buf)
                    
            return True
        else :
            return False
        
    def _get_raw_names (self) :
        #   Try to open tar file
        if self.verbose :
            sys.stderr.write ("Opening: %s\n" % self.df.basefile)
            
        try :
            self.df.fh = tarfile.open (self.df.basefile)
        except Exception, e :
            sys.stderr.write ("%s\n" % e)
            
        #   Get tar file 'members' and sort them
        members = self.df.fh.getmembers ()
        members.sort (self._member_cmp)
        self.df.subfiles = members
        