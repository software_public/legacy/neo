#!/usr/bin/env python
#
#   Read and write neo config file(s)
#   Config file is a key, value pair on each line
#
#   Steve Azevedo, August 2005
#

import os, os.path, sys, string

class configs :
    def __init__ (self, file) :
        self.file = file
        self.rcdict = {}

    def read (self) :
        tmp = {}
        if os.path.exists (self.file) :
            fh = open (self.file)
        else :
            return

        if not fh :
            sys.stderr.write ("Warning: failed to open %s\n" % file)
            return

        while 1 :
            line = fh.readline ()
            if not line : break
            if line[0] == '#' : continue
            flds = string.split (line)

            if len (flds) >= 2 :
                tmp[flds[0]] = flds[1]

        fh.close ()
        if tmp :
            self.rcdict = tmp

    def write (self) :
        fh = open (self.file, 'w')
        if not fh :
            sys.stderr.write ("Warning: failed to open %s for write\n" % file)
            return

        for key, value in self.rcdict.items () :
            fh.write ("%s\t%s\n" % (key, value))

        fh.close ()

    def add (self, **kw) :
        #print kw
        for key, value in kw.items () :
            self.rcdict[key] = value

    def remove (self, key) :
        if self.rcdict.has_key (key) :
            del self.rcdict[key]
        

if __name__ == "__main__" :
    '''   Test Stub   '''
    con = configs ('./neo.config')
    ldict = {}
    dum = []
    ldict['DEFAULT_MODE'] = 'MANUAL'
    ldict['DEFAULT_SOURCE'] = '/'
    ldict['DEFAULT_DEST'] = './'
    ldict['DEFAULT_FORMAT'] = 'FORMAT_TAR'
        
    con.add (**ldict)
    con.write ()
    #con.write ()
