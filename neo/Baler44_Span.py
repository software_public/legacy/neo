#!/usr/bin/env picpython

#
#   Library to generate a time span file from BALER44 data
#
#   Steve Azevedo, June 2009
#

import os, sys, subprocess, re
import Stats, TimeDoy, Un

PROG_VERSION = '2009.162a'

MSVIEW = 'msview '

#   Filenames per data loggers
baler44DataRE = re.compile (".*data\/[A-Z]{2}-.*2\d{13}")

class Msview_Info (object) :
    __slots__ = 'time_string', 'samples', 'sample_rate', 'seconds'
    def __init__ (self, time_string, samples, sample_rate, seconds) :
        self.time_string = time_string
        self.samples = samples
        self.sample_rate = sample_rate
        self.seconds = seconds

class Baler44_Span :
    def __init__ (self) :
        self.packet_info = {}
        self.netstachans = []
        
    def clear (self) :
        self.packet_info = {}
        self.netstachans = []        
        
    def read (self, filename) :
        '''   Read the output from msview   '''
        archive_type = guess_type (filename)
        if archive_type == None : return
        if archive_type == 'ZIP' :
            reader = Un.neoZIP (filename)
        elif archive_type == 'tar' :
            reader = Un.neoTAR (filename)
            
        reader.open ()
        while 1 :
            if not reader.read (baler44DataRE) : break
            if not reader.buf.buf : continue
            
            try :
                fh = subprocess.Popen (MSVIEW + ' - ' + " 2>&1", shell = True, stdout = subprocess.PIPE, stdin = subprocess.PIPE, close_fds = True)
                lines, err = fh.communicate (reader.buf.buf)
                #fh.stdin.write (reader.buf.buf)
            except Exception, e :
                sys.stderr.write ("Error: exception opening file: %s\n" % e)
                continue
            
            if not fh : break
            lines = lines.split ('\n')
            for line in lines :
                if not line : continue
                flds = line.split (',')
                if len (flds) < 9 : continue
                #print len (flds)
                netstachan = flds[0].strip ()
                samples = flds[4].strip (); samples = samples.split ()[0]
                sample_rate = flds[5].strip (); sample_rate = sample_rate.split ()[0]
                time_string = line.split ()[8].strip ()
                try :
                    seconds = float (samples) / float (sample_rate)
                except :
                    seconds = 0.0
                    
                #print samples, sample_rate, seconds
                mi = Msview_Info (time_string, samples, sample_rate, seconds)
                if not self.packet_info.has_key (netstachan) :
                    self.packet_info[netstachan] = []
                    
                self.packet_info[netstachan].append (mi)
                #self.netstachans.append (netstachan)
                #except Exception, e :
                    #sys.stderr.write ("Error: Failed to parse output from msview: %s\n" % e)
                    #continue
             
            #fh.terminate ()
            
        self.netstachans = self.packet_info.keys ()
        self.netstachans.sort ()
        
    def time_span (self, a) :
        first_time_string = a[0].time_string
        last_time_string = a[len (a) - 1].time_string
        seconds_to_add = a[len (a) - 1].seconds
        
        first_time_epoch = epoch_from_time_string (first_time_string)
        last_time_epoch = epoch_from_time_string (last_time_string) + seconds_to_add
        
        #print first_time_string, first_time_epoch, last_time_string, seconds_to_add, last_time_epoch
        
        days = float (last_time_epoch - first_time_epoch) / 86400.
        
        return days
        
    def write (self, outfile) :
        if len (self.netstachans) < 1 :
            return False
        
        fh = open (outfile, 'a+')
        
        for k in self.netstachans :
            a = self.packet_info[k]
            a.sort (sort_on_time_string)
            time_span_days = self.time_span (a)
            fh.write ("\n%s\n" % k)
            time_span_data = 0.0
            
            total_secs = 0.0
            use_time = None
            for l in a :
                if use_time == None :
                    use_time = l.time_string
                    
                total_secs += l.seconds
                    
            time_span_data = total_secs / 86400.
            if use_time != None :
                fh.write ("\t%s\t%12.3f\n" % (use_time, total_secs))
                #print ("\t%s\t%12.3f" % (use_time, total_secs))
                
            fh.write ("\t---------------------------------------------\n")
            fh.write ("\tTotal Days -> Span: %7.2f\tData: %7.2f\n" % (time_span_days, time_span_data))
                          
        fh.close ()
        return True
    
###   Mixins   ###
#   Guess file type based on suffix
def guess_type (filename) :
    #   Its a directory so it must be raw data
    if os.path.isdir (filename) :
        return None
    
    suffix = filename[-3:]
    if suffix == 'ZIP' or suffix == 'tar' :
        return suffix
    else :
        return None
    
def sort_on_time_string (x, y) :
    return cmp (x.time_string, y.time_string)

def epoch_from_time_string (s) :
    tdoy = TimeDoy.TimeDoy ()
    yr, doy, rest = s.split (',')
    hr, mn, secs = rest.split (':')
    
    return tdoy.epoch (int (yr), int (doy), int (hr), int (mn), int (float (secs) + 0.5))

if __name__ == '__main__' :
    bs = Baler44_Span ()
    files = os.listdir ('/media/BALER44/data')
    for f in files :
        f = os.path.join ('/media/BALER44/data', f)
        bs.read (f)

    bs.write ("junk.txt")