#!/usr/bin/env picpython
#
#   A program to read rt-130 CF cards and Q330S memory sticks
#   -- Can read multiple cards at once
#   -- Can output RefTek, tar, or zip formats
#   -- Can automatically detect and mount CF cards
#   -- Has packet browser, to trouble shoot data
#
#   convert class to handle conversions
#   element class to create GUI elements
#   mounts class to handle mounts
#   packets class to create packet browser GUI
#
#   Steve Azevedo, August 2005
#
#   Version 2006.352
#   Made alterations to mounts.py for compatibility
#   issues with FC5, FC6 and Macintosh.  mounts.py now
#   uses df instead of fstab and mstab to find mounted
#   cf cards.  Also made some changes as to which mount
#   command is used for compatibility with FC5, FC6 and Mac.

PROG_VERSION = '2018.179'
#   Where we find the PCMCIA status file is
#   Should actually check all common locations for portability
STAB = '/var/lib/pcmcia/stab'

import Tkinter, tkFileDialog, tkMessageBox, os, os.path, sys, string, Pmw
import convert, element, mounts, packets, configs, cfgGUI, Help, Files_Q330S, Files_RT130


TRUE  = (0 == 0)
FALSE = (0 == 1)

AUTO   = 0
MANUAL = 1

REFTEK = element.FORMAT_RT
TAR    = element.FORMAT_TAR
ZIP    = element.FORMAT_ZIP
RT130  = element.RT130
Q330S  = element.Q330S


DEFAULT_SOURCE = '/'
DEFAULT_DEST   = os.environ['HOME']
FMT = ZIP
DIGITIZER = RT130
GENL = False

CONFIG = os.path.join (os.environ['HOME'], 'neo.rc')

#
#   Mount elements
#   This is a dictionary of element keyed on source directory
#
ME = {}

class wrap (element.element) :
    '''   Wrap element so we can add and override :^)  '''
    def __init__ (self, root, **kw) :
        apply (element.element.__init__, (self, root), kw)
        #   Add signal/slot for done button
        #self.doneButton.signal_add ('ButtonRelease', self.lunmount)
        #   Add signal/slot for convert button
        self.convButton.signal_add ('ButtonRelease', self.offload)
        #   This holds an instance of mounts.mounts
        self.mounts = None
        #   This is an instance of convert, and set up status bar
        self.convert = convert.convert ()
        self.convert.set_mess (self.mess)
        self.convert.set_root (self)
            
        #print "FMT ", FMT
        #self.format_var.set (FMT)
        self.sourceEntry.bind ("<Return>", self.readSource)
        self.destinationEntry.bind ("<Return>", self.readDestination)
        self.log_mess = Pmw.TextDialog (root, title = "Last log message")
        self.log_mess.withdraw ()

    def log (self) :
        mess = self.convert.get_log ()
        if mess == "" :
            self.log_mess.configure (title = "*** No log messages ***")
        else :
            self.log_mess.configure (title = "Last log messages")
           
        self.log_mess.clear ()
        self.log_mess.insert (Tkinter.END, mess)
        self.log_mess.activate (globalMode = 'nograb')
        self.log_mess.tkraise ()

    #   Override here
    def getSource (self) :
        if os.path.exists (self.source) :
            src = self.source
        else :
            src = DEFAULT_SOURCE
            
        self.source = tkFileDialog.askdirectory (
            title = "Source directory",
            parent = self.root,
            initialdir = src)

        if not self.source :
            self.source = src

        self.sourceEntry.delete (0, Tkinter.END)
        self.sourceEntry.insert (0, self.source)

    #   From entry widget
    def readSource (self, a) :
        s = self.sourceEntry.get ()
        if os.path.exists (s) :
            self.source = s

        self.sourceEntry.delete (0, Tkinter.END)
        self.sourceEntry.insert (0, self.source)        

    #   Override here
    def getDestination (self) :
        if os.path.exists (self.destination) :
            dest = self.destination
        else :
            dest = DEFAULT_DEST
            
        self.destination = tkFileDialog.askdirectory (
            title = "Destination directory",
            parent = self.root,
            initialdir = dest)
        
        if not self.destination : return
        try :
            if os.access (self.destination, os.W_OK) :
                self.destinationEntry.delete (0, Tkinter.END)
                self.destinationEntry.insert (0, self.destination)
            else :
                import tkMessageBox
                tkMessageBox.showerror ("Error:", "%s not writable." % self.destination)
                self.destination = dest
        except TypeError :
            pass

    #   From entry widget
    def readDestination (self, a) :
        s = self.destinationEntry.get ()
        if os.path.exists (s) :
            self.destination = s

        self.destinationEntry.delete (0, Tkinter.END)
        self.destinationEntry.insert (0, self.destination)        

    #
    #   Offload the data
    #
    def offload (self) :
        global GENL
        if not os.path.exists (self.source) :
            mess = "Wake up! %s does not exist" % self.source
            tkMessageBox.showerror ("Asleep at the wheel", mess)
            return

        if not os.path.exists (self.destination) :
            mess = "Wake up! %s does not exist" % self.destination
            tkMessageBox.showerror ("Asleep at the wheel", mess)
            return
            
        self.mess.set ("Calculating file sizes...")
        self.convert.set_src (self.source)
        #print "set src"
        self.convert.set_dest (self.destination)
        #print "set dest"
        #print GENL
        self.convert.set_gen_log (GENL)
        #print "set gen"
        DIGITIZER = self.digitizer.get ()
        #print DIGITIZER
        if DIGITIZER == RT130 :
            self.convert.set_File (Files_RT130)
            #print "set File"
        elif DIGITIZER == Q330S :
            self.convert.set_File (Files_Q330S)

        fmt = self.format_var.get ()
        if fmt == element.FORMAT_RT :
            ret = self.convert.make_reftek ()
        elif fmt == element.FORMAT_TAR :
            #print "call make_tar"
            ret = self.convert.make_tar ()
        elif fmt == element.FORMAT_ZIP :
            #print fmt
            ret = self.convert.make_zip ()
            
        if ret == False :
            if DIGITIZER == RT130 :
                d = 'RT-130'
            else :
                d = 'BALER_44'
                
            self.mess.set ("Not files found for %s" % d)

    #
    #   Set source (CF card) directory
    #
    def setSrc (self, src) :
        self.source = src
        self.sourceEntry.delete (0, Tkinter.END)
        self.sourceEntry.insert (0, self.source)

    #
    #   Where we write the data
    #
    def setDest (self, dest) :
        self.destination = dest
        self.destinationEntry.delete (0, Tkinter.END)
        self.destinationEntry.insert (0, self.destination)

    #
    #   Find out which PCMCIA socket the CF card is in
    #
    def setSocket (self) :
        self.psock = 'USB'
        if not self.source : return
        device = self.mounts.device (self.source)
        dev = os.path.basename (device)[0:3]
        
        if os.path.exists (STAB) :
            stab = STAB
        else :
            return

        fh = open (stab)
        if not fh :
            sys.write.stderr ("Failed to open %s\n" % stab)
            return
        
        while 1 :
            line = fh.readline ()
            #print line
            if not line : break
            flds = string.split (line)
            try :
                if flds[4] == dev :
                    self.psock = flds[0]
            except :
                pass

    #
    #   Holds mounts instance
    #
    def setMounts (self, mounts) :
        self.mounts = mounts

    def del_me (self, source) :
        #sys.stderr.write ("del_me (%s)\n" % source)
        del ME[source]

    #
    #   Unmount and destroy
    #
    def lunmount (self) :
        #import tkMessageBox
        #print ME
        self.convert.die = TRUE
        if self.source :
            #   Try clean unmount first
            if not self.mounts.umount (self.source) :
                #   Failed, so hammer it
                if not self.mounts.force (self.source) :
                    mess = "Failed to un-mount, is device busy?"
                    tkMessageBox.showwarning ("Unmounting error", mess)
                    return
            
            mess = "Remove: %s From socket %s, Then press 'OK'" % \
                   (self.source, self.psock)
            tkMessageBox.showinfo ("Remove disk", mess)
            
            if ME.has_key (self.source) :
                #   Wait 7 seconds before accepting new card
                self.root.after (7000, lambda s = self, k = self.source:
                                 s.del_me (k))
            
#
###############################################################################
#
class app (Tkinter.Frame) :
    '''   The main application   '''
    
    def __init__ (self, root, **kw) :
        apply (Tkinter.Frame.__init__, (self, root), kw)

        self.cfg = configs.configs (CONFIG)

        #   The top status bar
        self.mess = element.StatusBar (self)
        self.mess.pack (anchor = Tkinter.N, expand = 1, fill = Tkinter.X)

        self.root = root
        self.genLogs = Tkinter.BooleanVar ()
        self.genLogs.set (False)
        #   mount points
        self.mp = []
        #   Generic counter
        self.i = 0

        self.makeMenu ()
        self.mounts = mounts.mounts ()
        self.checkMounts ()
        self.readConfig ()

        #   
        self.root.after (2500, self.resetTitle)
        
    def setGENL (self) :
        global GENL
        GENL = self.genLogs.get ()
        #print GENL

    def makeMenu (self) :
        self.mb = Tkinter.Menu (self.root)
        fm = Tkinter.Menu (self.mb)
        fm.add_command (label = "New", command = self.makeNew)
        fm.add_separator ()
        fm.add_command (label = 'Quit', command = self.gobyebye)
        self.mb.add_cascade (label = 'File', menu = fm)

        #   We still need help menu and mode menus
        self.modeVar = Tkinter.IntVar ()
        om = Tkinter.Menu (self.mb)
        om.add_radiobutton (label = 'Automatic', value = AUTO,
                            variable = self.modeVar)
        om.add_radiobutton (label = 'Manual', value = MANUAL,
                            variable = self.modeVar)
        self.modeVar.set (MANUAL)

        om.add_separator ()
        om.add_checkbutton (label = 'Generate log files', variable = self.genLogs, command = self.setGENL, state = Tkinter.DISABLED)
        
        om.add_separator ()
        om.add_command (label = "Configure...", command = self.doConfig)
        self.mb.add_cascade (label = 'Mode', menu = om)

        hm = Tkinter.Menu (self.mb)
        hm.add_command (label = 'Manual mode...', command = self.helpManual)
        hm.add_command (label = 'Automatic mode...', command = self.helpAuto)
        self.mb.add_cascade (label = 'Help', menu = hm)

        self.root.config (menu = self.mb)

    def helpManual (self) :
        try :
            h = Help.Help (self.root, MANUAL)
            h.activate ()
        except :
            pass

    def helpAuto (self) :
        try :
            h = Help.Help (self.root, AUTO)
            h.activate ()
        except :
            pass

    def gobyebye (self) :
        self.root.quit ()

    def resetTitle (self) :
        self.root.title ('Neo ' + PROG_VERSION)

    def readConfig (self) :
        global DEFAULT_SOURCE, DEFAULT_DEST, FMT, DIGITIZER
        self.cfg.read ()
        for key, value in self.cfg.rcdict.items () :
            #print key, value
            if key == 'DEFAULT_SOURCE' :
                DEFAULT_SOURCE = value
                #print "DS " + DEFAULT_SOURCE
            elif key == 'DEFAULT_DEST' :
                DEFAULT_DEST = value
                #print "DD " + DEFAULT_DEST
            elif key == 'DEFAULT_FORMAT' :
                if value == 'Tar' :
                    FMT = TAR
                #elif value == 'RefTek' :
                    #FMT = REFTEK
                elif value == 'Zip' :
                    FMT = ZIP
            elif key == 'DEFAULT_MODE' :
                if value == 'Automatic' :
                    self.modeVar.set (AUTO)
                else :
                    self.modeVar.set (MANUAL)
            elif key == 'GENERATE_LOGS' :
                if value == 'True' :
                    self.genLogs.set (True)
                else :
                    self.genLogs.set (False)
            elif key == 'DEFAULT_DIGITIZER' :
                if value == 'RT-130' :
                    DIGITIZER = RT130
                elif value == 'Q330-S' :
                    DIGITIZER = Q330S

    def doConfig (self) :
        top = Tkinter.Toplevel ()
        cfgGUI.cfgGUI (top)
        

    #
    #   Create a new element
    #
    def makeNew (self, src = None, dest = None) :
        
        e = wrap (self, relief = Tkinter.RIDGE, bd = 3, bg = 'OliveDrab')
        e.pack (expand = 1, fill = Tkinter.X, anchor = Tkinter.W)
        
        #   Set mounts instance in element
        e.setMounts (self.mounts)

        #   Keep track of each instance keyed on source (CF) directory
        if not src == None :
            e.setSrc (src)
            ME[src] = e
            #print ME
        else :
            #print DEFAULT_SOURCE
            e.setSrc (DEFAULT_SOURCE)

        #   Set the destination directory in element
        if not dest == None :
            e.setDest (dest)
        else :
            e.setDest (DEFAULT_DEST)

        #print "FMT ", FMT
        e.format_var.set (FMT)
        #print DIGITIZER
        e.digitizer.set (DIGITIZER)
        #   We were called from the menu so make a manual element
        if src == None or dest == None :
            return

        #   Find which pcmcia socket we are in if any
        if self.modeVar.get () == AUTO :
            e.setSocket ()
            e.doneButton.signal_add ('ButtonRelease', e.lunmount)

    #
    #   Have we done this one already?
    #
    def is_used (self, m) :
        #print "ME " + self.mp
        for e in ME.values () :
            src = e.source
            if src == m :
                return TRUE

        return FALSE
        
    #
    #   Check for new CF card every 3000 ms
    #
    def checkMounts (self) :
        import tkMessageBox
        self.mp = []
        self.i += 1
        
        #   Come back here in 3000 ms
        self.after (3000, self.checkMounts)
        if self.modeVar.get () == MANUAL :
            self.mess.set ("Manual mode")
            return
        
        self.mess.set ("Checking mounts...%d" % self.i)
        #   Read fstab and mtab
        self.mounts.read ()
        for p in self.mounts.mount_points () :
            #   Are we a USB or an IDE in the PCMCIA?
            if mounts.cfRE.match (p) :
                self.mp.append (p)
        #   Go thru the mount points and see if they are already used
        for m in self.mp :
            if self.is_used (m) :
                continue
            
            self.mess.set ("New data media at: %s" % m)
            if not self.mounts.is_mounted (m) :
                if not self.mounts.mount (m) :
                    mess = "Failed to mount: %s" % m
                    tkMessageBox.showwarning ("Mounting error", mess)
                    return
            self.makeNew (m, DEFAULT_DEST)
def main():
    root = Tkinter.Tk ()
    root.title ("Neo: Hm, Upgrades.")
    root.resizable (1, 0)
    a = app (root, width = 100)
    a.pack (expand = 1, fill = Tkinter.X, anchor = Tkinter.N)

    root.mainloop ()

if __name__ == '__main__' :
    main()