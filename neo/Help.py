#!/usr/bin/env python

from Tkinter import *
import Pmw

helpMessage = []


hm = '''
        
                                  Automatic Mode

1.Select automatic mode by selecting the menu item "Automatic" under the
mode menu. At present this only works under linux.

2.Insert the CF card in the USB card reader or in the PCMCIA slot with an
adapter. Note: The 1.0 GB SimpleTech cards can only be read in the USB card
reader and not in the PCMCIA slot. After a period of time, (30 seconds worst
case), a message "New CF card at:" will appear and a new instance of the
offload tool will be created.

3.The correct source should already be selected. You can select the output
directory by selecting the "Destination:" button which will bring up a file
dialog.

4.Select the output format, either "RefTek", "Tar", or "Zip".

5.Press the "Convert" button to start the transfer.

6.You can also select "File" -> "New" to bring up another instance of the
offload tool. Select the same source but a different output format, in
order to create another copy of the data in a different format.

7.Once the offload is complete, click the "Done" button and follow the
directions given by the dialog.

8.Warning, warning, warning. It is very important not to remove the CF card
while it is still mounted. This will cause the computer to hang.

9.Note: To save the format that the offload tool comes up in select
"Mode" -> "Configure".

Further Help:

        Please report any problems or send suggestions to:
        
                  e-mail: passcal@passcal.nmt.edu
                  phone: 505.835.5070
                  
                                ==========
'''

helpMessage.append (hm)

hm = '''
                                 Manual Mode


1.Insert and mount the CF card using the computers OS.

2.Select "File" -> "New" menu to bring up an instance of the offload tool.

3.On the offload tool click the "Source:" button to bring up a file dialog
to select the location of the CF card, /media/usbdisk as an example.

4.Now, select the "Destination:" button to select the directory where the
data will get written.

5.Select the radio button for the output format that you want, "RefTek",
"Tar", or "Zip".

6.Press the "Convert" button to begin the transfer. The status bar will
show "Idle.." followed by the status of the transfer.

7.Un-mount the CF card using the computers OS.

8.Warning, warning, warning. It is very important not to remove the CF card
while it is still mounted. This will cause the computer to hang.

9.Note: To save the format that the offload tool comes up in select
"Mode" -> "Configure".

Further Help:

        Please report any problems or send suggestions to:
        
                  e-mail: passcal@passcal.nmt.edu
                  phone: 505.835.5070
                  
                                ----------
'''

helpMessage.append (hm)

class Help :
    def __init__ (self, root, n) :
        self.help = Pmw.TextDialog (root, title = "Help dialog")
        self.help.withdraw ()
        self.help.insert (END, helpMessage[n])
        self.help.configure (text_state = DISABLED)
        

    def activate (self) :
        #print helpMessage
        self.help.activate (globalMode = 'nograb')
        self.help.tkraise ()

if __name__ == '__main__' :
    #from Tkinter import *
    def hheellpp () :
        h.activate ()
    r = Tk ()
    h = Help (r)
    b = Button (r, text = 'help', command = hheellpp)
    b.pack ()
    r.mainloop ()
