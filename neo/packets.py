#!/usr/bin/env python
#
#   A packet browser widget for rt-130's and Q330-S's.
#
#   Entry point is display:
#
#      display.display (root-window, root-directory)
#
#   Steve Azevedo, August 2005
#
PROG_VERSION = '2009.151'

import os, Tree, string, sys, re
import Tkinter

refRE = re.compile (".*\d{9}_.{8}")
qRE = re.compile (".*\w\w-.*-\d{14}")

REFPACKET = "refpacket -f "
MSVIEW = "msview -p "

LISTBOX = None
MESS    = None

class menuBar (Tkinter.Frame) :
    def __init__ (self, root, **kw) :
        apply (Tkinter.Frame.__init__, (self, root), kw)

        self.root = root

        self.start = 1.0
        self.str = ""

        self.mb = Tkinter.Menu (self)
        filemenu = Tkinter.Menu (self.mb)
        filemenu.add_command (label = "Open...", command = self.open,
                              state = Tkinter.DISABLED)
        filemenu.add_command (label = "Quit", command = self.gobyebye)
        self.mb.add_cascade (label = 'File', menu = filemenu)

        specialmenu = Tkinter.Menu (self.mb)
        specialmenu.add_command (label = 'Search forward...',
                                 command = self.searchforward)
        specialmenu.add_command (label = 'Find again <Clt-s>',
                                 command = self.searchagain)
        self.mb.add_cascade (label = 'Search', menu = specialmenu)

        self.root.config (menu = self.mb)
	
	self.root.bind ('<Control-s>', self.searchagain)

    def notfound (self) :
        import tkMessageBox
        tkMessageBox.showwarning ("Search failed",
                                  "\"%s\" not found!" % self.str)
        self.start = 1.0
        self.str = ""

    def setTag (self, pos, len) :
        l, c = string.split (pos, ".")
        pos2 = l + "." + str (int (c) + len)
        LISTBOX.tag_add (Tkinter.SEL, pos, pos2)

    def searchforward (self) :
        self.start = 1.0
        LISTBOX.tag_remove (Tkinter.SEL, 1.0, Tkinter.END)
        from tkSimpleDialog import askstring
        ret = askstring ("Search String", "Search for:", parent = self.root)
        if ret == None : return
        self.str = ret
        pos = LISTBOX.search (ret, self.start, stopindex = Tkinter.END)
        if pos == "" :
            self.notfound ()
            pos = str (self.start)
            
        #print pos
        LISTBOX.see (pos)
        self.setTag (pos, len (self.str))
        self.start = str (float (pos) + 0.1)
        #print self.start

    def searchagain (self, e = None) :
        #print self.str, self.start
        if self.str == "" :
            self.notfound ()
            return
        
        pos = LISTBOX.search (self.str, self.start, stopindex = Tkinter.END)
        if pos == "" :
            self.notfound ()
            pos = str (self.start)
            
        LISTBOX.see (pos)
        self.setTag (pos, len (self.str))
        self.start = str (float (pos) + 0.1)

    def open (self) :
        pass

    def gobyebye (self) :
        self.root.destroy ()

class fileMessage (Tkinter.Frame) :
    def __init__ (self, root, **kw) :
        apply (Tkinter.Frame.__init__, (self, root), kw)

        self.mess = Tkinter.StringVar ()
        Tkinter.Label (self, textvariable = self.mess,
                       font = ("Courier", 10)).pack (
            anchor = Tkinter.W)

    def message (self, mess) :
        self.mess.set (mess)
        self.update_idletasks ()

class nodeDisplay (Tree.Node) :
    ''' Display file using refpacket
        disp.listbox must be global
    '''
    def __init__ (self, *args, **kw) :
        apply (Tree.Node.__init__, (self,) + args, kw)
        #   Call back for mouse click on in tree display
        self.widget.tag_bind (self.symbol, '<1>', self.showPackets)
        self.widget.tag_bind (self.label,  '<1>', self.showPackets)

        self.lb = LISTBOX
        
    #
    #   Display output from refpacket in listbox
    #
    def showPackets (self, e) :
        #   Get path to file
        afile = ''
        for e in self.full_id () :
            afile = os.path.join (afile, e)

	#print afile
	if refRE.match (afile) :
	    conv_prog = REFPACKET
	elif qRE.match (afile) :
	    conv_prog = MSVIEW
	else :
	    return
	
        try :
            size = os.stat (afile)[6]
            s = "Size: " + str (size) + " bytes"
            MESS (s)
        except :
            sys.stderr.write ("Can't stat: " + afile)

        #   Enable edits so we can insert
        LISTBOX.configure (state = Tkinter.NORMAL)
        #   Clear listbox
        LISTBOX.delete (1.0, Tkinter.END)
        #   Run refrate
        try :
	    afile = afile.replace (' ', '\ ')
            fh = os.popen (conv_prog + afile + " 2>&1")
            #print fh
        except :
            sys.stderr.write (
                "External program refpacket not found, can not continue!\n")
            sys.exit ()

        if not fh : return
        #   Fill listbox
        while 1 :
            line = fh.readline ()
            if not line : break
            #line = line[:-1]
            line = string.expandtabs (line)
            LISTBOX.insert (Tkinter.END, line)

        #   Disable edits
        LISTBOX.configure (state = Tkinter.DISABLED)
        fh.close ()

class packetDisplay (Tkinter.Frame) :
    '''   Our listbox   '''
    def __init__ (self, root, **kw) :
        apply (Tkinter.Frame.__init__, (self, root), kw)
        
        sbv = Tkinter.Scrollbar (self, orient=Tkinter.VERTICAL)
        sbh = Tkinter.Scrollbar (self, orient=Tkinter.HORIZONTAL)
        self.listbox = Tkinter.Text (self, yscrollcommand=sbv.set,
                                     xscrollcommand=sbh.set,
                                     width = 80,
				     bg = 'LightYellow')
        sbv.config(command=self.listbox.yview)
        sbh.config(command=self.listbox.xview)

        self.listbox.grid (row = 0, column = 0, sticky = 'nsew')
        self.grid_rowconfigure (0, weight = 1)
        self.grid_columnconfigure (0, weight = 1)

        sbv.grid (row = 0, column = 1, sticky = 'ns')
        sbh.grid (row = 1, column = 0, sticky = 'ew')

        #self.listbox.root.expand ()

class treeDisplay (Tkinter.Frame) :
    '''   File tree display   '''
    def __init__ (self, root, dir, **kw) :
        apply (Tkinter.Frame.__init__, (self, root), kw)

        self.tree = Tree.Tree (master = self,
                               root_id = dir,
                               root_label = dir,
                               get_contents_callback = self.getfiles,
                               width = 225,
                               node_class = nodeDisplay,
			       bg = 'LightYellow')

        self.tree.grid (row = 0, column = 0, sticky = 'nsew')
        self.grid_rowconfigure (0, weight = 1)
        self.grid_columnconfigure (0, weight = 1)

        sbv = Tkinter.Scrollbar (self, orient = Tkinter.VERTICAL)
        sbv.grid (row = 0, column = 1, sticky = 'ns')
        self.tree.configure (yscrollcommand = sbv.set)
        sbv.configure (command = self.tree.yview)

        sbh = Tkinter.Scrollbar (self, orient = Tkinter.HORIZONTAL)
        sbh.grid (row = 1, column = 0, sticky = 'ew')
        self.tree.configure (xscrollcommand = sbh.set)
        sbh.configure (command = self.tree.xview)
        
        self.tree.focus_set (); self.tree.root.expand ()

    def getfiles (self, node) :
        path=apply(os.path.join, node.full_id())
        for filename in os.listdir(path):
            full=os.path.join(path, filename)
            name=filename
            folder=0
            if os.path.isdir(full):
                # it's a directory
                folder=1
            elif not os.path.isfile(full):
                # but it's not a file
                name=name+' (special)'
                
            if os.path.islink(full):
                # it's a link
                name=name+' (link to '+os.readlink(full)+')'
                
            self.tree.add_node(name=name, id=filename, flag=folder)


def display (root, path) :
    global LISTBOX, MESS
    root.title ("rt-130 packet display")
    menuBar (root).pack ()
    f = Tkinter.Frame (root)
    disp = packetDisplay (f)
    disp.pack (expand = 1, fill = Tkinter.BOTH, side = Tkinter.RIGHT)
    LISTBOX = disp.listbox
    tree = treeDisplay (f, path)
    tree.pack (expand = 1, fill = Tkinter.BOTH, side = Tkinter.RIGHT)
    f.pack (expand = 1, fill = Tkinter.BOTH)
    mess = fileMessage (root)
    mess.pack (side = Tkinter.BOTTOM)
    MESS = mess.message
        

if __name__ == "__main__" :
    try :
        path = sys.argv[1]
    except :
        path = './data'
        
    print path
    
    root = Tkinter.Tk ()
    display (root, path)
    
    root.mainloop ()
