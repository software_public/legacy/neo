#!/usr/bin/env python
#
#   Read fstab and mtab to determine mount status.
#   Cleanly mount and umount disks that are hotplugged.
#
#   Steve Azevedo, August 2005
#
#   2006.352
#   Reconfigured mounts.py to use df instead of checking
#   mstab and fstab to find cf cards.  Also made some 
#   changes for the way mounting and unmounting is used
#   for compatibility with FC5, FC6 and Macintosh.
#
PROG_VERSION = "2018.179"

import string, sys, time, os, re
uname_info = os.uname()
ostype = uname_info[0]
if ostype == "Darwin" :
    MOUNT = '/usr/sbin/diskutil mount '
    UMOUNT = '/usr/sbin/diskutil unmount '
    FORCE = MOUNT + "force "
else :
    MOUNT  = '/bin/mount '
    UMOUNT = '/bin/umount '
    FORCE  = '/bin/umount -l '
    s = os.uname()
    s = s[2]
    if re.search("FC", s) or re.search("fc", s) or re.search ("el", s) :
        MOUNT = '/usr/bin/gnome-mount -p '
        UMOUNT = '/usr/bin/gnome-mount -up '
        #FORCE = '/bin/umount -l'

FILE = 0
TAB  = 1

TRUE  = (1 == 1)
FALSE = (0 == 1)

#   RE to match hotplugged usb or ide disks or RefTek labled disks
if ostype == "Darwin" :
    cfRE = re.compile ('(/Volumes/usbdisk)|(/Volumes/idedisk)|(/Volumes/RT130)|(/Volumes/NO NAME)|(/Volumes/BALER44)')
else :
    HOMENAME1 = os.path.join ('/', 'media', os.path.basename (os.environ['HOME']), 'RT130')
    HOMENAME = os.path.join ('/', 'run', 'media', os.path.basename (os.environ['HOME']), 'RT130')
    cfRE = re.compile ('(/media/usbdisk)|(/media/idedisk)|(/media/RT130)|(/media/disk)|(/media/BALER44)|({0})|({1})'.format (HOMENAME,HOMENAME1))
    #cfRE = re.compile (HOMENAME)

class dftab :
    '''   fstab structure   '''
    def __init__ (self) :
        self.df_fsys = None
        self.df_1kblcks = None
        self.df_used = None
        self.df_avail = None
        self.df_cap = None
        self.df_mntpnt = None


class mounts :
    '''   simple class that parses output of df   '''

    def __init__ (self) :
        self.init_tabs ()

    def init_tabs (self) :
        self.dftab = []
        self.dftab.append ('')
        self.dftab.append ({})

    #   parse output of df
    def read_df (self, tab) :
        if ostype == 'Linux' :
            fh = os.popen ('df -l -k -P')
        else :
            fh = os.popen ('df -l -k -P')
        if not fh :
            return
        while 1 :
            line = fh.readline ()
            if not line :
                break

            line = line[:-1]
            flds = string.split (line)
            if len (flds) == 0 :
                continue

            if flds[0] == '#' :
                continue
            if len (flds) > 6:
                i = 6;
                while i < len(flds):
                    flds[5] = flds[5] + ' ' + flds[i]
                    i += 1
            #   Key on mount point
            key = flds[5]

            #   Save all of the fields
            t = dftab ()
            try :
                t.df_fsys = flds[0]
                t.df_1kblcks = flds[1]
                t.df_used = flds[2]
                t.df_avail = flds[3]
                t.df_cap = flds[4]
                t.df_mntpnt = flds[5]
            except LookupError :
                sys.stderr.write ("Warning: LookupError: %s in: %s\n" %
                                  (line, file))
                pass

            tab[TAB][key] = t

        fh.close ()

    #   Read df
    def read (self) :
        self.init_tabs ()
        self.read_df (self.dftab)

    #   Return a list of mount points 
    def mount_points (self) :
        return self.dftab[TAB].keys ()

    #   For a given directory see if it is mounted
    def is_mounted (self, path) :
        for m in self.dftab[TAB].keys () :
            if m == path :
                return TRUE

        return FALSE

    def device (self, path) :
        if self.dftab[TAB].has_key (path) :
            return self.dftab[TAB][path].df_fsys
        else :
            return None

    def mount (self, path) :
        command = MOUNT + path + " 2>&1 > /dev/null" 
        for i in range (5) :
            if os.path.exists (path) :
                break
            time.sleep (0.5)
        else :
            return FALSE

        dev = self.device (path)
        for i in range (15) :
            if os.path.exists (dev) :
                break
            time.sleep (5)

        else :
            return FALSE

        for i in range (30) :
            ret = os.system (command)
            if ret == 0 :
                return TRUE
            else :
                sys.stderr.write ("Warning: mount returns %d\n" % ret)

            time.sleep (2)

        return FALSE


    def umount (self, path) :
        path = path.replace(' ', '\ ')
        command = UMOUNT + path + " 2>&1 > /dev/null"
        for i in range (3) :
            ret = os.system (command)
            if ret == 0 :
                return TRUE
            else :
                sys.stderr.write ("Warning: umount returns %d\n" % ret)

            time.sleep (2)

        return self.force (path)

    def force (self, path) :
        command = FORCE + path + " 2>&1 > /dev/null"
        for i in range (3) :
            ret = os.system (command)
            if ret == 0 or ret == 512 :
                return TRUE
            else :
                sys.stderr.write ("Error: umount returns %d\n" % ret)

            time.sleep (2)

        return FALSE

    #   Debug
    def print_it (self) :
        print '***dftab***'
        for k in self.dftab[TAB].keys () :
            print self.dftab[TAB][k].df_mntpnt

if __name__ == '__main__' :
    mts = []
    m = mounts ()
    m.read ()
    for f in m.mount_points () :
        #print f,
        if m.is_mounted (f) :
            print " Mounted ", m.device (f)
            if cfRE.match (f) :
                print f, 'matches...'
        else :
            print " Unmounted", m.device (f)
            if cfRE.match (f) :
                if m.mount (f) :
                    mts.append (f)
    time.sleep (10)
    m.read ()
    for f in m.mount_points () :
        print f,
        if m.is_mounted (f) :
            print " Mounted ", m.device (f)
        else :
            print " Unmounted", m.device (f)

    for f in mts :
        m.umount (f)



