#!/usr/bin/env python
#
#   Do conversions: rt130 -> .ref, rt130 -> tar, rt130 -> zip
#
#   Steve Azevedo, August 2005
#

PROG_VERSION = '2018.179'

RT130CUT = 'rt130cut -g -r '
TAR = 'tar --bzip2 --totals -cvf '
ZIP = 'zip '
GEN = 'rt130cut -S -r '
LOG = 'ref2log -f -'

#   'RT130' or 'Q330'
DIGITIZER = None

SIZE = (1 << 31) - 1   #   Maximum size of zip file data
TIMEOUT = 60.0 * 30.0   #   Timeout on zip thread join

import os, os.path, re, string, time, sys
from threading import *
from Queue import Queue
import Files_Q330S, Files_RT130, Baler44_Span, RT130_Span, Zip, Tar, Ref

#   Regular expression to match date/das from path to data files
cfRE = re.compile (".*(2\d{6})[/](\w\w\w\w).*")
dayRE = re.compile (".*(2\d{6}[/])9\w\w\w.*")
idleRE = re.compile ("Idle.*")
logRE = re.compile ("Log.*")

TRUE  = (1 == 1)
FALSE = (1 == 0)

class convert :
    def __init__ (self, src = None, dest = None) :
        #   Source directory
        self.src = src
        #   Destination directory
        self.dest = dest
        #   These are callbacks to element gui
        self.root = None
        self.mess = None
        self.log = ""
        self.message = ""
        self.gen_log = False
        self.threads = []
        self.spanner = None
        self.queue = Queue (250)
        
    def set_File (self, Files) :
        self.Files = Files
        #print Files.TYPE
        if Files.TYPE == 'Q330S' :
            #print "Set spanner Q330"
            self.spanner = Baler44_Span.Baler44_Span ()
        elif Files.TYPE == 'RT130' :
            #print "Set spanner RT130"
            self.spanner = RT130_Span.RT130_Span ()
        
    def die (self) :
        #print "Die"
        for t in self.threads :
            t.kill ()
            
        self.threads = []

    def get_log (self) :
        return self.log
    
    def set_gen_log (self, what) :
        self.gen_log = what

    def set_src (self, src) :
        #self.src = src.replace(' ', '\ ')
        self.src = src

    def set_dest (self, dest) :
        self.dest = dest

    def set_mess (self, mess) :
        self.mess = mess

    def set_root (self, root) :
        self.root = root

    #   Get output file name from directory tree on CF
    #def outname (self, src = None) :
        #if src == None :
            #src = self.src
            
        #for root, dirs, files in os.walk (src) :
            ##print dirs
            #for n in files :
                #p = os.path.join (root, n)
                #if cfRE.match (p) :
                    #m = cfRE.match (p)
                    ##print m.groups ()
                    #return string.join (m.groups (), '.')

        #return "recovered" + str (int (time.time ()))

    def updateMessage (self) :
        #print "updateMessage"
        try :
            self.root.update ()
            buf = self.queue.get (True, 400)
            self.mess.set (str (buf))
            self.log += buf + '\n'
            if buf[0] != 'I' :
                self.root.after (250, self.updateMessage)
        except :
            pass

    def append_gen_log (self) :
        comm = '(cd ' + self.dest + '; ' + GEN + self.src + ' | ' + LOG + ') 2>&1'
        return comm

    #   Build up a command to make .ref file from CF
    def make_reftek (self) :
        r = Ref.Ref (self.src, self.dest, self)
        t = r.start ()
        self.threads.append (r)

    #   Build up a command to make .tar.bz2 file from CF
    def make_tar (self) :
        #self.set_mess ("Calculating time spans...")
        fl = self.Files.Files (self.src, self.dest, SIZE, spanner = self.spanner)
        fl.start ()
        self.threads.append (fl)
        while fl.isAlive () :
            self.root.update ()
            time.sleep (0.3)
        #fl.join (30.0)
        #fl.read ()
        #fl.assign_group ()
        #start_dir = os.getcwd ()
        #if not self.spanner.write (os.path.join (self.dest, "time_spans.txt")) :
            #return False
        
        while 1 :
            gp = fl.get_next_group ()
            if not gp : break
            of = string.split (gp[0].name, os.path.sep)
            of = os.path.join (self.dest, "%s.%s.tar" % (of[0], of[1]))
            base = gp[0].base
            f = []
            for g in gp :
                f = f + g.files
                
            #os.chdir (base)
            if not f : continue
            #self.queue = Queue (250)
            z = Tar.Tar (f, of, base, self.queue)
            t = z.start ()
            self.threads.append (z)
            self.updateMessage ()
            
        #os.chdir (start_dir)
        return True

    #   Build up a comand to make .ZIP file from CF
    def make_zip (self) :
        #self.set_mess ("Calculating time spans...")
        #print "Enter..."
        fl = self.Files.Files (self.src, self.dest, SIZE, spanner = self.spanner)
        fl.start ()
        self.threads.append (fl)
        #print "Join..."
        #fl.join (30.0)
        while fl.isAlive () :
            self.root.update ()
            time.sleep (0.3)
            #print '.'
        #fl.read ()
        #fl.assign_group ()
        #start_dir = os.getcwd ()
        #if not self.spanner.write (os.path.join (self.dest, "time_spans.txt")) :
            #return False
        #print "Run zip..."
        
        zipIn = []
        while True :
            gp = fl.get_next_group ()
            #
            if not gp : break
            of = string.split (gp[0].name, os.path.sep)
            of = os.path.join (self.dest, "%s.%s.ZIP" % (of[0], of[1]))
            base = gp[0].base
            f = []
            for g in gp :
                f = f + g.files
                
            #os.chdir (base)
            if not f : continue
            zipIn.append ([f, of, base])
            #self.queue = Queue (250)
        for zz in zipIn :
            #z = Zip.Zip (f, of, base, self.queue)
            z = Zip.Zip (zz[0], zz[1], zz[2], self.queue)
            #print "Returned", z.name
            t = z.start ()
            #print "Started", z.name
            self.threads.append (z)
            self.updateMessage ()
            #z.join ()
            #while z.isAlive () :
                ##print z.name
                #self.updateMessage ()
                #time.sleep (.05)
            #time.sleep (.5)
            #self.updateMessage ()
        
        #os.chdir (start_dir)
        #for t in self.threads () : t.kill ()
        return True

if __name__ == '__main__' :
    import Tkinter
    import element
    c = convert ('/home/azevedo/Desktop/RT130-9404', '/home/azevedo/Desktop')
    root = Tkinter.Tk ()
    e = element.element (root)
    e.pack ()
    c.set_mess (e.mess)
    c.set_root (root)
    #c.make_reftek ()
    #print c.outname ()
    #c.make_tar ()
    #c.make_zip ()
    #   Testing callback buttons
    t = Tkinter.Toplevel (root)
    Tkinter.Button (t, text = "Test Zip", command = c.make_zip).pack ()
    Tkinter.Button (t, text = "Test Tar", command = c.make_tar).pack ()
    Tkinter.Button (t, text = "Test Reftek", command = c.make_reftek).pack ()
    
    root.mainloop ()

    
