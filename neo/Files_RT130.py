#!/usr/bin/env python

#
#   Create a group of files that are no larger than a given size.
#   For the RT130 data logger
#

import os, os.path, sys, re
from threading import *
import Lock

PROG_VERSION = '2010.336'
TYPE = 'RT130'

#   Keep the day directories
dirRE = re.compile ("^2\d\d\d\d\d\d")
#
spannerRE = re.compile (".*\/[1-9]\/.*")

class DirFile (object) :
    '''   Structure to keep track of files by size   '''
    __slots__ = ('size', 'files', 'name', 'group', 'base')
    def __init__ (self, sz, nm, base, group = None) :
        self.size = sz
        self.name = nm
        self.group = group
        self.base = base
        self.files = []
        
    def append (self, file) :
        self.files = self.files + file

class Files (Thread) :
    def __init__ (self, src_dir, dest_dir, sz, spanner = None) :
        Thread.__init__ (self)
        self.root = src_dir
        self.dest = dest_dir
        #   Current group
        self.current = 0
        sz = str (sz)
        #   kilobytes
        if sz[-1] == 'k' :
            self.size = int (float (sz[:-1])) * 1024
        #   megabytes
        elif sz[-1] == 'm' :
            self.size = int (float (sz[:-1])) * 1024 * 1024
        #   gigabytes
        elif sz[-1] == 'g' :
            self.size = int (float (sz[:-1])) * 1024 * 1024 * 1024
        #   bytes
        else :
            self.size = int (float (sz))
            
        self.top = []
        self.lock = Lock.lock ().LOCK
        
        self.spanner = spanner
            
    def read (self) :
        def build (arg, dir, names) :
            namedirs = {}
            files = []
            total = 0
            
            HaveFiles = False
            for n in names :
                f = os.path.join (dir, n)
                if os.path.isdir (f) :
                    continue
                
                HaveFiles = True
                try :
                    statinfo = os.stat (f)
                    total += statinfo.st_size
                    #   ***
                    #   Need to generate start and end times file here
                    #   ***
                    #f = os.path.join (arg, f)
                    #if spannerRE.match (f) and self.spanner :
                        #self.spanner.read (f)
                        
                    files.append (f)
                except OSError, e :
                    sys.stderr.write ("Error: The file %s has a low level problem. Skipping\n" % f)
            
            if not HaveFiles : return
            dirfile = DirFile (total, dir, self.root)
            dirfile.append (files)
            
            self.top.append (dirfile)
        
        #try :
        #self.lock.acquire ()
        start_dir = os.getcwd ()
        os.chdir (self.root)
        BASEDIR = os.path.basename (self.root)
        dirs = os.listdir ('.')
        dirs.sort ()
        for d in dirs :
            if dirRE.match (d) :
                os.path.walk (d, build, self.root)
            else :
                if d == 'archive.sta' :
                    sz = os.stat (d).st_size
                    dirfile = DirFile (sz, BASEDIR, self.root)
                    dirfile.append ([d])
                    self.top.append (dirfile)
                    
        os.chdir (start_dir)
        #finally :
            #self.lock.release ()
                    
    def assign_group (self) :
        group = 0
        total = 0
        for t in self.top :
            total += t.size
            #print total,
            if total >= self.size :
                group += 1
                total = t.size
                t.group = group
            else :
                t.group = group
                
            #print group
                
    def rewind (self) :
        self.current = 0
         
    def get_next_group (self, what = None) :
        group = []
        if what == None :
            what = self.current
        
        for t in self.top :
            if t.group == what :
                group.append (t)
                
        if len (group) != 0 :
            self.current += 1
        else :
            self.rewind ()
            group = None
            
        return group
    
    def kill (self) :
        pass
    
    def run (self) :
        try :
            self.lock.acquire ()
            self.read ()
            self.assign_group ()
            #self.spanner.write (os.path.join (self.dest, "time_spans.text"))
        finally :
            self.lock.release ()
            
        sys.exit ()
    
if __name__ == "__main__" :
    import RT130_Span
    #SIZE = 1024 * 1024 * 0.5   #   One half MB
    SIZE = (1 << 31) - 1   #   2GB
    fl = Files ('/Volumes/RT130-913B', '.', str (SIZE), RT130_Span.RT130_Span ())
    print "Reading...",
    fl.read ()
    print
    print "Assigning...",
    fl.assign_group ()
    print
    
    while 1 :
        g = fl.get_next_group ()
        if g == None : break
        for ge in g :
            print ge.name, ge.group, ge.size
            for f in ge.files :
                print "\t", f