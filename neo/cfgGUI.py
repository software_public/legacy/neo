#!/usr/bin/env python
#
#   A configuration GUI for neo
#
#   Steve Azevedo, August 2005
#

import Tkinter, configs, os, os.path
import Pmw, tkFileDialog, tkMessageBox

PROG_VERSION = '2009.150'

#   Default location of configuration file
RCFILE = os.path.join (os.environ['HOME'], 'neo.rc')

#   Default source directory
DEFAULT_SOURCE = 'DEFAULT_SOURCE'
#   Default destination directory
DEFAULT_DEST   = 'DEFAULT_DEST'
#   Default output format (RefTek, Tar, Zip)
DEFAULT_FORMAT = 'DEFAULT_FORMAT'
#   Default mode (Automatic, Manual)
DEFAULT_MODE   = 'DEFAULT_MODE'
#   Default digitizer (RT-130, Q330-S)
DEFAULT_DIGITIZER = 'DEFAULT_DIGITIZER'

class cfg (configs.configs) :
    '''   We inherit from configs   '''
    def __init__ (self, file) :
        apply (configs.configs.__init__, (self, file))
        #   Location of rt 130 data
        self.source = '/'
        #   Where we write the data
        self.destination = '/'
        #   Manual or Automatic
        self.mode = 'Manual'
        #   Zip or Tar
        self.format = 'Zip'
        #   RT-130 or Q330-S
        self.digitizer = 'RT-130'

    #   Sync configs
    def update (self) :
        ldict = {}
        ldict[DEFAULT_SOURCE] = self.source
        ldict[DEFAULT_DEST]   = self.destination
        ldict[DEFAULT_MODE]   = self.mode
        ldict[DEFAULT_FORMAT] = self.format
        ldict[DEFAULT_DIGITIZER] = self.digitizer

        self.add (**ldict)

class cfgGUI (Tkinter.Frame) :
    '''   Da GUI   '''
    def __init__ (self, root, **kw) :
        apply (Tkinter.Frame.__init__, (self, root), kw)


        self.root = root
        Pmw.initialise (root)
        root.title ("Set Default Configuration")

        #print RCFILE
        self.cfg = cfg (RCFILE)
        #
        #   Source and Destination
        #
        f1 = Tkinter.Frame (self, relief = Tkinter.RIDGE, bd = 3)

        Tkinter.Button (f1, text = "Source:",
                        command = self.readSrc).grid (row = 0,
                                                      column = 0,
                                                      sticky = 'ew')
        self.srcLabel = Tkinter.Label (f1, relief = Tkinter.FLAT,
                                       anchor = Tkinter.W, width = 32)
        self.srcLabel.grid (row = 0, column = 1, sticky = 'ew')
        f1.columnconfigure (0, weight = 1)

        Tkinter.Button (f1, text = "Destination:",
                        command = self.readDest).grid (row = 1,
                                                       column = 0,
                                                       sticky = 'ew')
        
        self.destLabel = Tkinter.Label (f1, relief = Tkinter.FLAT,
                                        anchor = Tkinter.W, width = 32)
        self.destLabel.grid (row = 1, column = 1, sticky = 'ew')
        f1.columnconfigure (1, weight = 1)
        f1.pack ()
        #
        #   Mode and Format
        #
        f2 = Tkinter.Frame (self, relief = Tkinter.RIDGE, bd = 3)

        self.modeVar = Tkinter.StringVar ()

        Pmw.OptionMenu (f2, labelpos = 'w', label_text = "     Mode:         ",
                        menubutton_textvariable = self.modeVar,
                        menubutton_width = 20,
                        items = ['Automatic', 'Manual']
                        ).pack (side = Tkinter.TOP,
                                fill = Tkinter.X)
        self.modeVar.set ('Manual')
        
        self.formatVar = Tkinter.StringVar ()

        Pmw.OptionMenu (f2, labelpos = 'w', label_text = "     Format:       ",
                        menubutton_textvariable = self.formatVar,
                        menubutton_width = 20,
                        items = ['Tar', 'Zip']
                        ).pack (side = Tkinter.TOP,
                                fill = Tkinter.X)
        self.formatVar.set ('Zip')
        
        self.digitizerVar = Tkinter.StringVar ()
        
        Pmw.OptionMenu (f2, labelpos = 'w', label_text = "     Digitizer:     ",
                        menubutton_textvariable = self.digitizerVar,
                        menubutton_width = 20,
                        items = ['RT-130', 'Q330-S']
                        ).pack (side = Tkinter.TOP,
                                fill = Tkinter.X)
        self.digitizerVar.set ('RT-130')
        
        f2.pack (fill = Tkinter.X)
        #
        #   Buttons
        #
        f3 = Tkinter.Frame (self, relief = Tkinter.RIDGE, bd = 3)

        Tkinter.Button (f3, text = 'Cancel',
                        command = self.doCancel).grid (row = 0, column = 0,
                                                  sticky = 'ew')
        Tkinter.Button (f3, text = 'Okay',
                        command = self.doSet).grid (row = 0, column = 1,
                                               sticky = 'ew')
        f3.columnconfigure (0, weight = 1)
        f3.columnconfigure (1, weight = 1)
        f3.pack (fill = Tkinter.X)

        self.readConfig ()
        self.setGUI ()
        self.pack ()

    def doCancel (self) :
        self.root.destroy ()

    #   Write config file
    def doSet (self) :
        self.cfg.mode   = self.modeVar.get ()
        self.cfg.format = self.formatVar.get ()
        self.cfg.digitizer = self.digitizerVar.get ()
        self.cfg.update ()
        self.cfg.write ()
        self.root.destroy ()
        tkMessageBox.showinfo ('Re-start Program',
                               "Re-start program to activate")
        
    #   Read the config file
    def readConfig (self) :
        self.cfg.read ()
        if self.cfg.rcdict :
            for key, value in self.cfg.rcdict.items () :
                if key == 'DEFAULT_SOURCE' :
                    self.cfg.source = value
                elif key == 'DEFAULT_DEST' :
                    self.cfg.destination = value
                elif key == 'DEFAULT_FORMAT' :
                    self.cfg.format = value
                elif key == 'DEFAULT_MODE' :
                    self.cfg.mode = value
                elif key == 'DEFAULT_DIGITIZER' :
                    self.cfg.digitizer = value

    def writeConfig (self) :
        self.cfg.write ()

    #   Set up stuff in GUI
    def setGUI (self) :
        if self.cfg.source :
            self.srcLabel.config (text = self.cfg.source)

        if self.cfg.destination :
            self.destLabel.config (text = self.cfg.destination)

        if self.cfg.format :
            self.formatVar.set (self.cfg.format)

        if self.cfg.mode :
            self.modeVar.set (self.cfg.mode)
            
        if self.cfg.digitizer :
            #print self.cfg.digitizer
            self.digitizerVar.set (self.cfg.digitizer)

    #   Read source
    def readSrc (self) :
        if os.path.exists (self.cfg.source) :
            src = self.cfg.source
        else :
            src = '/'
            
        self.cfg.source = tkFileDialog.askdirectory (
            title = "Source directory",
            parent = self.root,
            initialdir = src)

        if not self.cfg.source :
            self.cfg.source = src
        
        self.srcLabel.config (text = "")
        self.srcLabel.config (text = self.cfg.source)

    #   Read destination
    def readDest (self) :
        if os.path.exists (self.cfg.destination) :
            dest = self.cfg.destination
        else :
            dest = '/'
            
        self.cfg.destination = tkFileDialog.askdirectory (
            title = "Destination directory",
            parent = self.root,
            initialdir = dest)

        if not self.cfg.destination :
            self.cfg.destination = dest
        
        self.destLabel.config (text = "")
        self.destLabel.config (text = self.cfg.destination)


if __name__ == '__main__' :
    '''   Test Stub   '''
    root = Tkinter.Tk ()
    c = cfgGUI (root)
    c.pack ()
    root.mainloop ()
