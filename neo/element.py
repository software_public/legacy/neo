#!/usr/bin/env python
#
#   GUI element for neo program
#
#   Steve Azevedo, August 2005
#

import Tkinter, tkFileDialog
#import mounts

PROG_VERSION = '2009.150'

#   RefTek
FORMAT_RT     = 1
#
RT130         = 1
#   Q330
Q330S         = 2
#   Tar
FORMAT_TAR    = 2
#   Zip
FORMAT_ZIP    = 4
#
#FORMAT_RT+TAR  = 3
#FORMAT_RT+ZIP  = 5

#
#   A dictionary of Tkinter event numbers keyed by event name
#
eventList = { 'KeyPress' : '2', 'KeyRelease' : '3', 'ButtonPress' : '4',
              'ButtonRelease' : '5', 'Motion' : '6', 'Enter' : '7',
              'Leave' : '8', 'FocusIn' : '9', 'FocusOut' : '10',
              'Expose' : '12', 'Visibility' : '15', 'Destroy' : '17',
              'Unmap' : '18', 'Map' : '19', 'Reparent' : '21',
              'Configure' : '22', 'Gravity' : '24', 'Circulate' : '26',
              'Property' : '28',  'Colormap' : '32','Activate' : '36',
              'Deactivate' : '37'}

class Registry :
    ''' Set up a crude signal slot mechanism   '''
    def __init__ (self) :
        self.connections = {}

    def add (self, occasion, function) :
        if eventList.has_key (occasion) :
            occasion = eventList[occasion]
        if self.connections.has_key (occasion) == 0 :
            self.connections[occasion] = [function]
        else :
            self.connections[occasion].append (function)

    def remove (self, occasion, function) :
        if self.connections.has_key (occasion) :
            self.connections[occasion].remove (function)

    def execute (self, occasion) :
        if self.connections.has_key (occasion) :
            for function in self.connections[occasion] :
                apply (function)
                

class StatusBar(Tkinter.Frame):
    '''   A simple status bar   '''
    def __init__(self, master):
        Tkinter.Frame.__init__(self, master)
        self.tv = Tkinter.StringVar ()
        self.label = Tkinter.Label(self, bd=1,
                                   relief=Tkinter.SUNKEN,
                                   anchor=Tkinter.W,
                                   width = 45, bg = 'white',
                                   textvariable = self.tv)
        self.label.pack(fill=Tkinter.X)

    def set(self, args):
        #print "set", args
        self.tv.set (args)
        #self.label.config(text=format % args)
        self.label.update_idletasks()

    def clear(self):
        self.tv.set ("")
        #self.label.config(text="")
        self.label.update_idletasks()

class Button (Tkinter.Button) :
    ''' Our button class   '''
    def __init__ (self, root, t=None, **kw) :
        # Sub-class Tkinter.Button
        #Tkinter.Button.__init__ (self, root)
        apply (Tkinter.Button.__init__, (self, root), kw)
        self.configure (text = t)
        self.register = Registry ()
        #print self.configure ()

    #  Add a signal/slot
    def signal_add (self, signal, slot) :
        self.bind ('<' + signal + '>', self.dispatch)
        self.register.add (signal, slot)

    #  Remove a signal/slot
    def signal_remove (self, signal, slot) :
        self.unbind ('<' + signal + '>')
        self.register.remove (signal, slot)

    #  Dispatch callback
    def dispatch (self, what) :
        #print self, what.type
        
        self.register.execute (what.type)


class element (Tkinter.Frame) :
    '''   The GUI element   '''
    def __init__ (self, root, **kw) :
        apply (Tkinter.Frame.__init__, (self, root), kw)

        self.root = root
        #self.mnts = mounts.mounts ()
        #   Set up source, destination stuff
        self.source = self.destination = None
        f1 = Tkinter.Frame (self, relief = Tkinter.GROOVE, bd = 2)

        self.srcButton = Button (f1, text = "Source:")
        self.srcButton.grid (row = 0, sticky = 'ew')
        self.srcButton.signal_add ('ButtonRelease', self.getSource)
        
        self.sourceEntry = Tkinter.Entry (f1, bg = 'LightYellow')

        self.destButton = Button (f1, text = "Destination:")
        self.destButton.grid (row = 1, sticky = 'ew')
        self.destButton.signal_add ('ButtonRelease', self.getDestination)
        
        self.destinationEntry = Tkinter.Entry (f1, bg = 'LightYellow')

        self.sourceEntry.grid (row = 0, column = 1, sticky = 'ew')
        self.destinationEntry.grid (row = 1, column = 1, sticky = 'ew')

        f1.grid (sticky = 'ew')
        f1.grid_columnconfigure (0, weight = 1)
        f1.grid_columnconfigure (1, weight = 1)

        f1.pack (expand = 1, fill = Tkinter.X)
        
        #   Set up output type
        f2 = Tkinter.Frame (self, relief = Tkinter.GROOVE, bd = 2)
        
        f21 = Tkinter.Frame (f2, bd = 1, relief = Tkinter.SOLID)
        
        self.digitizer = Tkinter.IntVar ()
        
        Tkinter.Radiobutton (f21,
                             text = 'RT-130',
                             variable = self.digitizer,
                             value = RT130).grid (column = 0, row = 0,
                                                      sticky = 'w')
        
        Tkinter.Radiobutton (f21,
                             text = 'Q330-S',
                             variable = self.digitizer,
                             value = Q330S).grid (column = 1, row = 0,
                                                      sticky = 'e')

        self.format_var = Tkinter.IntVar ()
        
        f22 = Tkinter.Frame (f2, bd = 1, relief = Tkinter.SOLID)
        '''
        Tkinter.Radiobutton (f2,
                             text = 'RefTek',
                             variable = self.format_var,
                             value = FORMAT_RT).grid (column = 0, row = 0,
                                                      sticky = 'w')
        '''
        Tkinter.Radiobutton (f22,
                             text = 'Tar   ',
                             variable = self.format_var,
                             value = FORMAT_TAR).grid (column = 0, row = 0,
                                                       sticky = 'w')
        #   This should most likely be FORMAT_TAR | FORMAT_RT
        Tkinter.Radiobutton (f22,
                             text = 'Zip   ',
                             variable = self.format_var,
                             value = FORMAT_ZIP).grid (column = 1, row = 0,
                                                       sticky = 'e')
        
        f21.grid_columnconfigure (0, weight = 1)
        f21.grid_columnconfigure (1, weight = 1)
        f21.grid_columnconfigure (2, weight = 1)
        #self.digitizer.set (RT130)
        f21.pack (side = Tkinter.LEFT, expand = 1, fill = Tkinter.X)
        
        f22.grid_columnconfigure (0, weight = 1)
        f22.grid_columnconfigure (1, weight = 1)
        f22.grid_columnconfigure (2, weight = 1)
        #self.format_var.set (FORMAT_ZIP)
        f22.pack (side = Tkinter.RIGHT, expand = 1, fill = Tkinter.X)
        
        f2.pack (expand = 1, fill = Tkinter.X)
        
        #   Control buttons
        f3 = Tkinter.Frame (self, relief = Tkinter.GROOVE, bd = 2)
        
        self.convButton = Button (f3, text = "Convert")
        self.convButton.grid (row = 0, column = 0, sticky = 'ew')
        #   Just a place holder
        self.convButton.signal_add ('ButtonRelease', self.convert)
        
        self.browseButton = Button (f3, text = "Browse", fg = "DarkSlateGray")
        self.browseButton.grid (row = 0, column = 1, sticky = 'ew')
        self.browseButton.signal_add ('ButtonRelease', self.packets)

        self.logButton = Button (f3, text = "Log", fg = "DarkSlateGray")
        self.logButton.grid (row = 0, column = 2, sticky = 'ew')
        self.logButton.signal_add ('ButtonRelease', self.log)
        
        self.doneButton = Button (f3, text = "Done", fg = 'Red')
        self.doneButton.grid (row = 0, column = 3, sticky = 'ew')
        self.doneButton.signal_add ('ButtonRelease', self.unmount)
                                              
        f3.grid_columnconfigure (0, weight = 1)
        f3.grid_columnconfigure (1, weight = 1)
        f3.grid_columnconfigure (2, weight = 1)
        f3.grid_columnconfigure (3, weight = 1)
        f3.pack (expand = 1, fill = Tkinter.X)

        #   Message bar
        self.mess = StatusBar (self)
        self.mess.pack (expand = 1, fill = Tkinter.X)
        self.mess.set ("Idle...")
        
    def convert (self) :
        pass

    def log (self) :
        pass

    def unmount (self) :
        self.convert.die ()
        self.destroy ()

    def packets (self) :
        if self.source == None :
            import tkMessageBox
            tkMessageBox.showwarning ("Warning",
                                      "Warning, no source")
            return
            
        import packets
        top = Tkinter.Toplevel (self)
        packets.display (top, self.source)

    def getSource (self) :
        self.source = tkFileDialog.askdirectory (
            title = "Source directory",
            parent = self.root,
            initialdir = '/media')

        self.sourceEntry.delete (0, Tkinter.END)
        self.sourceEntry.insert (0, self.source)


    def getDestination (self) :
        self.destination = tkFileDialog.askdirectory (
            title = "Destination directory",
            parent = self.root,
            initialdir = '/')

        self.destinationEntry.delete (0, Tkinter.END)
        self.destinationEntry.insert (0, self.destination)


if __name__ == "__main__" :
    root = Tkinter.Tk ()
    root.title ("Neo: Hm, Upgrades.")
    e = element (root, relief = Tkinter.RIDGE, bd = 2)
    e.pack (expand = 1,
            fill = Tkinter.X,
            #side = Tkinter.RIGHT,
            anchor = Tkinter.NW)
    
    e1 = element (root, relief = Tkinter.RIDGE, bd = 2)
    e1.pack (expand = 1,
             fill = Tkinter.X,
             #side = Tkinter.RIGHT,
             anchor = Tkinter.NW)

    root.mainloop ()
