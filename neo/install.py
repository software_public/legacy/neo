#!/usr/bin/env picpython

import shutil, os, stat, sys
from compileall import *

#   Location of PASSCAL distribution
ROOTDIR = '/opt/passcal'
#
if len(sys.argv) > 1 :
    ROOTDIR = sys.argv[1]
elif os.environ.has_key ('PASSCAL') :
    ROOTDIR = os.environ['PASSCAL']
else :
    sys.stderr.write ("PASSCAL environment variable not set!\n")
    sys.exit ()

PROGDIR = os.getcwd ()
PROG = os.path.basename (PROGDIR)
LIBDIR = ROOTDIR + '/lib/python'
BINDIR = ROOTDIR + '/bin'
LIBPROG = LIBDIR + '/' + PROG

PROGS = ('neo','refscrub', 'gents')

LIBS  = ('Baler44_Span',
         'cfgGUI',
         'configs',
         'convert',
         'element',
         'Files_Q330S',
         'Files_RT130',
         'gents',
         'Lock',
         'main',
         'mounts',
         'packets',
         'Ref',
         'RT130_Span',
         'Tar',
         'Tree',
         'Un',
         'refscrub',
         'Zip')

#   Delete libs
for p in LIBS :
    p = p + '.pyc'
    try :
        os.remove (p)
    except OSError :
        pass
#   Compile
compile_dir (".")
#   Make libs dir
command = 'mkdir -p ' + LIBDIR
os.system (command)
#   Remove old libs
try :
    shutil.rmtree (LIBPROG)
except OSError :
    pass

#   install libraries
shutil.copytree (PROGDIR, LIBPROG)

command = 'mkdir -p ' + BINDIR
os.system (command)

#   install programs
for p in PROGS :
    src = p
    dst = BINDIR + '/' + p
    try :
        os.remove (dst)
    except OSError :
        pass
    print src, dst
    shutil.copy (src, dst)
    os.chmod (dst, 0557)
