#!/usr/bin/env python

#
#   Generate ref file in a thread so we can read the CF card in parallel.
#
#   Steve Azevedo, October 2007
#

from threading import *
import os, os.path, sys, string, time, re, signal

#   XXX
RT130CUT = 'rt130cut -g'
pidRE = re.compile ("PID:.*")

class Ref (Thread) :
    '''
        Class to generate a ref file in a thread:
          indir => Path to CF card archive
          outdir => The output directory
          caller => The message logging object
    '''
    def __init__ (self, indir, outdir, caller = None) :
        Thread.__init__ (self)
        self.indir = indir
        self.outdir = outdir
        self.die = False
        self.command = self.open ()
        self.caller = caller
        
    def kill (self) :
        self.die = True
        
    def open (self) :
        return "(cd %s; %s -r %s) 2>&1" % (self.outdir, RT130CUT, self.indir)
        
    def run (self) :
        #   Get start time
        then = time.time ()
        #
        pid = None
        #   Do we have a message logger?
        if self.caller :
            self.caller.message = self.command
            self.caller.log = self.caller.log + self.caller.message + "\n"
            self.caller.updateMessage ()
        else :
            print self.command
         
        #
        try :
            fh = os.popen (self.command)
        except :
            return
        
        if not fh : return
        while 1 :
            
            if self.die == True :
                if pid :
                    os.kill (pid, signal.SIGKILL)
                break
            
            line = fh.readline ()
            if not line :
                time.sleep (3)
                break
            
            line = line[:-1]
            
            if pidRE.match (line) :
                pid = int (string.split (line, ':')[1])
            
            if not len (line) : continue

            if self.caller :
                self.caller.message = line
                self.caller.log = self.caller.log + self.caller.message + "\n"
                self.caller.updateMessage ()
            else :
                print line
        
        fh.close ()
        now = time.time ()
        used = " tt: %5.2f minutes" % ((now - then) / 60)
        #print used
        if self.caller :
            self.caller.updateMessage ()
            self.caller.message = "Idle..." + used
            self.caller.log = self.caller.log + self.caller.message + "\n"
            self.caller.updateMessage ()
        else :
            print "Idle..." + used
            
        sys.exit ()
        
                
if __name__ == '__main__' :
    
    r = Ref ('/home/azevedo/Desktop/RT130-9404', '/home/azevedo/Desktop')
    
    r.start ()

