#!/usr/bin/env python

#
#   Generate tar file in a thread so we can read the CF card in parallel.
#
#   Steve Azevedo, October 2007
#

from tarfile import TarFile
from threading import *
import os, os.path, sys, string, time, Lock

PROG_VERSION = "2018.179"

class Tar (Thread) :
    '''
        Class to generate a tar file in a thread:
          infiles => A list of files to put in the tar file
          outfile => The tar file name
          queue => The message logging queue
    '''
    def __init__ (self, infiles, outfile, base, queue = None) :
        Thread.__init__ (self)
        self.infiles = infiles
        self.outfile = outfile
        self.die = False
        self.tar = self.open ()
        self.base = base
        self.queue = queue
        self.home = os.environ['HOME']
        self.lock = Lock.lock ().LOCK
        
    def open (self) :
        i = 0
        dir = os.path.dirname (self.outfile)
        while os.path.exists (self.outfile) :
            base = os.path.basename (self.outfile)
            flds = string.split (base, '.')
            self.outfile = os.path.join (dir, "%s.%02d.tar" % (string.join (flds[:2], "."), i))
            i += 1
            if i > 100 : return None
            
        try :
            return TarFile (self.outfile, 'w')
        except :
            return None
        
    def kill (self) :
        self.die = True
        
    def run (self) :
        self.lock.acquire ()
        #   Get start time
        then = time.time ()
        #   Do we have a message logger?
        if self.queue :
            self.queue.put ("Starting tar...", self.name)
        else :
            print "Starting tar..."
         
        #   Loop through files and add them to tar archive
        for f in self.infiles :
            #   Should we kill this thread?
            if self.die :
                self.lock.release ()
                self.die = False
                self.tar.close ()
                sys.exit ()
                break
            
            #   Add the file to the archive
            #try :
            os.chdir (self.base)
            try :
                self.tar.add (f)
            except IOError, e :
                if self.queue :
                    self.queue.put ("Error: Failed to write %s" % f)
                    
                sys.stderr.write ("Error: Failed to write %s\n" % f)                    
                
            os.chdir (self.home)
            #finally :
                #self.lock.release ()
                
            if self.queue :
                self.queue.put ("adding: %s" % f)
            else :
                print "adding: %s" % f
        
        self.tar.close ()
        now = time.time ()
        used = " tt: %5.2f minutes %s" % (((now - then) / 60), self.name)
        #print used
        if self.queue :
            self.queue.put ("Idle..." + used)
        else :
            print "Idle..." + used
        
        self.lock.release ()    
        sys.exit ()
        
    #def build (self) :
        #mythread = Thread (target = self.go)
        #mythread.start ()
        
        #return mythread
                
if __name__ == '__main__' :
    import Files
    start_dir = os.getcwd ()
    
    fl = Files.Files ('/home/azevedo/Desktop/RT130-9404', 1024 * 1024 * 1024 * 2.0)
    fl.read ()
    fl.assign_group ()
    
    #then = time.time ()
    for n in range (1) :
        while 1 :
            gp = fl.get_next_group ()
            if not gp : break
            of = string.split (gp[0].name, os.path.sep)
            of = "%s.%s.tar" % (of[0], of[1])
            of = os.path.join ('/home/azevedo/Desktop', of)
            base = gp[0].base
            #print of
            f = []
            for g in gp :
                f = f + g.files
            
            print "Writing %s..." % of
            os.chdir (base)
            z = Tar (f, of)
            z.start ()
            #z.join ()
            
    #now = time.time ()
    
    #print now - then
