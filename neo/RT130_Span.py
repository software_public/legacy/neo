#!/usr/bin/env picpython

#
#   Library to generate a time span file from rt-130 raw data
#
#   Steve Azevedo, June 2009
#

import os, sys, subprocess, re
import Stats, TimeDoy, Un

PROG_VERSION = '2009.162a'

REFPACKET = 'refpacket -f '

#   Filenames per data loggers
dasDataRE = re.compile (".*[1-9]\/\w{9}_\w{8}$")

class Refpacket_Info (object) :
    __slots__ = 'time_string', 'samples', 'sample_rate', 'seconds'
    def __init__ (self, time_string, samples, sample_rate, seconds) :
        self.time_string = time_string
        self.samples = samples
        self.sample_rate = sample_rate
        self.seconds = seconds

class RT130_Span :
    def __init__ (self) :
        self.packet_info = {}
        #self.netstachans = []
        self.dasstrmchans = []
        
    def clear (self) :
        self.packet_info = {}
        self.dasstrmchans = []        
        
    def read (self, filename) :
        '''   Read the output from refpacket   '''
        archive_type = guess_type (filename)
        if archive_type == None : return
        if archive_type == 'ZIP' :
            reader = Un.neoZIP (filename)
        elif archive_type == 'tar' :
            reader = Un.neoTAR (filename)
            
        reader.open ()
        while 1 :
            if not reader.read (dasDataRE) : break
            if not reader.buf.buf : continue
            
            try :
                fh = subprocess.Popen (REFPACKET + ' - ' + " 2>&1", shell = True, stdout = subprocess.PIPE, stdin = subprocess.PIPE, close_fds = True)
                lines, err = fh.communicate (reader.buf.buf)
                #fh.stdin.write (reader.buf.buf)
            except Exception, e :
                sys.stderr.write ("Error: exception opening file: %s\n" % e)
                continue
            
            if not fh : break
            lines = lines.split ('\n')
            lines.reverse ()
            have_DT = False
            while 1 :
                if not lines : break
                line = lines.pop ()
                if not line : continue
                flds = line.split ()
                if len (flds) < 2 : continue
                
                if flds[1] == 'DT' :
                    time_string = flds[6]
                    das = flds[8]
                    line = lines.pop ()
                    if not line : break
                    line = line.strip ()
                    if not line : continue
                    flds = line.split ()
                    have_DT = True           
                    if flds[0] == 'ns' :
                        samples = flds[2]
                        data_stream = flds[8]
                        channel = flds[11]
                    
                if flds[0] == 'stream' :
                    data_stream = flds[2]
                    continue
            
                if flds[1] == 'rate' :
                    sample_rate = flds[3]
                    continue
                    
                if have_DT :
                    have_DT = False
                    dasstrmchan = "%s_%s_%s" % (das, data_stream, channel)
                    seconds = float (samples) / float (sample_rate)
                    
                    ri = Refpacket_Info (time_string, samples, sample_rate, seconds)
                    if not self.packet_info.has_key (dasstrmchan) :
                        self.packet_info[dasstrmchan] = []
                        
                    self.packet_info[dasstrmchan].append (ri)
             
        #fh.close ()
        self.dasstrmchans = self.packet_info.keys ()
        self.dasstrmchans.sort ()
        
    def time_span (self, a) :
        first_time_string = a[0].time_string
        last_time_string = a[len (a) - 1].time_string
        seconds_to_add = a[len (a) - 1].seconds
        
        first_time_epoch = epoch_from_time_string (first_time_string)
        last_time_epoch = epoch_from_time_string (last_time_string) + seconds_to_add
        
        #print first_time_string, first_time_epoch, last_time_string, seconds_to_add, last_time_epoch
        
        days = float (last_time_epoch - first_time_epoch) / 86400.
        
        return days   
        
    def write (self, outfile) :
        if len (self.dasstrmchans) < 1 :
            return False
        
        fh = open (outfile, 'a+')
        
        for k in self.dasstrmchans :
            a = self.packet_info[k]
            a.sort (sort_on_time_string)
            time_span_days = self.time_span (a)
            fh.write ("%s\n" % k)
            time_span_data = 0.0
                            
            total_secs = 0.0
            use_time = None
            for l in a :
                if use_time == None :
                    use_time = l.time_string
                    
                total_secs += l.seconds
                
            time_span_data = total_secs / 86400.
            if use_time != None :
                fh.write ("\t%s\t%12.3f\n" % (use_time, total_secs))
                #print ("\t%s\t%12.3f" % (use_time, total_secs))
                
            fh.write ("\t---------------------------------------------\n")
            fh.write ("\tTotal Days -> Span: %7.2f\tData: %7.2f\n" % (time_span_days, time_span_data))
                          
        fh.close ()
        return True
        
    
###   Mixins   ###
#   Guess file type based on suffix
def guess_type (filename) :
    #   Its a directory so it must be raw data
    if os.path.isdir (filename) :
        return None
    
    suffix = filename[-3:]
    if suffix == 'ZIP' or suffix == 'tar' :
        return suffix
    else :
        return None
    
def sort_on_time_string (x, y) :
    return cmp (x.time_string, y.time_string)

def epoch_from_time_string (s) :
    tdoy = TimeDoy.TimeDoy ()
    yr, doy, hr, mn, sc, ms = s.split (':')
    
    secs = int (float (int (sc)) + (float (ms) / 1000.) + 0.5)
    
    return tdoy.epoch (int (yr) + 2000, int (doy), int (hr), int (mn), secs)

if __name__ == '__main__' :
    bs = RT130_Span ()
    files = os.listdir ('/media/disk/2009146/98BE/2')
    for f in files :
        f = os.path.join ('/media/disk/2009146/98BE/2', f)
        bs.read (f)

    bs.write ("temp.txt")