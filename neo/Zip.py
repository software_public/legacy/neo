#!/usr/bin/env python

#
#   Generate zip file in a thread so we can read the CF card in parallel.
#
#   Steve Azevedo, October 2007
#

from zipfile import ZipFile
from threading import *
import os, os.path, sys, string, time, Lock

PROG_VERSION = "2018.179"

class Zip (Thread) :
    '''
        Class to generate a zip file in a thread:
          infiles => A list of files to put in the zip file
          outfile => The zip file name
          #caller => The message logging object
          #message_buffer => The message buffer to display in the main thread
    '''
    #def __init__ (self, infiles, outfile, base, caller = None) :
    def __init__ (self, infiles, outfile, base, queue = None) :
        #print "Message buffer " + message_buffer
        Thread.__init__ (self)
        self.infiles = infiles
        self.outfile = outfile
        self.die = False
        self.zip = self.open ()
        self.base = base
        #self.caller = caller
        #self.message_buffer = message_buffer
        self.home = os.environ['HOME']
        self.lock = Lock.lock ().LOCK
        self.queue = queue
        
    def open (self) :
        i = 0
        dir = os.path.dirname (self.outfile)
        while os.path.exists (self.outfile) :
            base = os.path.basename (self.outfile)
            flds = string.split (base, '.')
            self.outfile = os.path.join (dir, "%s.%02d.ZIP" % (string.join (flds[:2], "."), i))
            i += 1
            if i > 100 : return None
            
        try :
            return ZipFile (self.outfile, 'w')
        except :
            return None
        
    def kill (self) :
        #print "Zip die"
        self.die = True
        
    def run (self) :
        self.lock.acquire ()
        #print "Acquired Lock", self.name, len (self.infiles)
        #   Get start time
        then = time.time ()
        #   Do we have a message logger?
        #if self.caller :
            #self.caller.message = "Starting zip..."
            #self.caller.log = self.caller.log + self.caller.message + "\n"
            #self.caller.updateMessage ()
        if self.queue :
            self.queue.put ("Starting zip...", self.name)
        else :
            print "Starting zip...", self.name
         
        #   Loop through files and add them to zip archive
        for f in self.infiles :
            #   Should we kill this thread?
            if self.die :
                #print "Killed...Releasing Lock"
                self.lock.release ()
                self.die = False
                self.zip.close ()
                sys.exit ()
                break
            
            #   Add the file to the archive
            #try :
            os.chdir (self.base)
            try :
                #print ".",
                self.zip.write (f)
            except IOError, e :
                #if self.caller :
                    #self.caller.message = "Error: Failed to write %s" % f
                    #self.caller.log = self.caller.log + self.caller.message + "\n"
                    #self.caller.updateMessage ()
                if self.queue :
                    self.queue.put ("Error: Failed to write %s" % f)
                    
                sys.stderr.write ("Error: Failed to write %s\n" % f)
                
            os.chdir (self.home)
            #finally :
                #self.lock.release ()
                
            #if self.caller :
                #self.caller.message = "adding: %s" % f
                #self.caller.log = self.caller.log + self.caller.message + "\n"
                #self.caller.updateMessage ()
            if self.queue :
                self.queue.put ("adding: %s" % f)
            else :
                print "adding: %s" % f
        
        self.zip.close ()
        now = time.time ()
        used = " tt: %5.2f minutes %s" % (((now - then) / 60), self.name)
        #print used
        #if self.caller :
            #self.caller.updateMessage ()
            #self.caller.message = "Idle..." + used
            #self.caller.log = self.caller.log + self.caller.message + "\n"
            #self.caller.updateMessage ()
        if self.queue :
            self.queue.put ("Idle..." + used)            
        else :
            print "Idle..." + used
        
        #print "Exiting...release Lock" 
        self.lock.release ()
        sys.exit ()
        
    #def build (self) :
        #mythread = Thread (target = self.go)
        #mythread.start ()
        
        #return mythread
                
if __name__ == '__main__' :
    import Files
    start_dir = os.getcwd ()
    
    fl = Files.Files ('/home/azevedo/Desktop/RT130-9404', 1024 * 1024 * 1024 * 2.0)
    fl.read ()
    fl.assign_group ()
    
    #then = time.time ()
    for n in range (1) :
        while 1 :
            gp = fl.get_next_group ()
            if not gp : break
            of = string.split (gp[0].name, os.path.sep)
            of = "%s.%s.ZIP" % (of[0], of[1])
            of = os.path.join ('/home/azevedo/Desktop', of)
            base = gp[0].base
            #print of
            f = []
            for g in gp :
                f = f + g.files
            
            print "Writing %s..." % of
            os.chdir (base)
            z = Zip (f, of)
            z.start ()
            #z.join ()
            
    #now = time.time ()
    
    #print now - then
