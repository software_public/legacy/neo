#!/usr/bin/env picpython

#
#   Program to create a time spams file from the output files from neo.
#
#   June 2009, Steve Azevedo
#

PROG_VERSION = '2009.162a Test version'

import sys, os, os.path, re, zipfile, tarfile
import Baler44_Span, RT130_Span

RT130 = 1
BALER44 = 2

#   Filenames per neo
dasRE = re.compile ("[9a-zA-Z][0-9a-zA-Z]{3}")
baler44RE = re.compile ("[A-Z]{2}-[A-Z0-9]{3,5}")

def guess_datalogger (filename) :
    '''   Guess at the type of datalogger based on file name   '''
    base = os.path.basename (filename)
    flds = base.split ('.')
    if len (flds) != 3 : return None
    if dasRE.match (flds[1]) :
        return RT130
    elif baler44RE.match (flds[1]) :
        return BALER44
    else :
        return None

def process_rt130 (filename) :
    #print "RT130"
    ts = RT130_Span.RT130_Span ()
    ts.read (filename)
    ts.write (os.path.join (os.path.dirname (filename), "time_spans.txt"))

def process_baler44 (filename) :
    #print "Baler44"
    ts = Baler44_Span.Baler44_Span ()
    ts.read (filename)
    ts.write (os.path.join (os.path.dirname (filename), "time_spans.txt"))

def read_directory (data_dir = None) :
    files = []
    if data_dir == None : data_dir = '.'
    if not os.path.isdir (data_dir) :
        return None

    for f in os.listdir (data_dir) :
        files.append (os.path.join (data_dir, f))

    return files

def get_args () :
    global DIR, FILE
    from optparse import OptionParser
    oparser = OptionParser ()

    oparser.usage = "gents -f data_file | -d data_directory\nVersion: %s" % PROG_VERSION
    oparser.description = "Read zip or tar files produced by neo and produce a time spans file."

    oparser.add_option ("-d", "--directory", dest = "data_directory",
                        help = "The directory containing the ZIP or tar files",
                        metavar = "data_directory")

    oparser.add_option ("-f", "--file", dest = "data_file",
                        help = "The ZIP or tar file",
                        metavar = "data_file")

    options, args = oparser.parse_args ()

    DIR = options.data_directory
    FILE = options.data_file

    if DIR == None and FILE == None :
        DIR = '.'

    if DIR != None and FILE != None :
        DIR = None


def main():
    global DIR, FILE
    get_args()
    if FILE != None:
        files = []
        files.append(FILE)
    else:
        files = read_directory(DIR)
        files.sort()

    for f in files:
        datalogger = guess_datalogger(f)
        print datalogger, f
        if datalogger == RT130:
            process_rt130(f)
        elif datalogger == BALER44:
            process_baler44(f)


if __name__ == '__main__' :
    main()
