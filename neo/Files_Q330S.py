#!/usr/bin/env python

#
#   Create a group of files that are no larger than a given size.
#   For the Q330S data logger (BALER44)
#

import os, os.path, sys, re, time
from threading import *
import Lock, TimeDoy

PROG_VERSION = '2009.175'
TYPE = 'Q330S'

#   All of the top level directories to keep
dirRE = re.compile ("^[a-z].*")
#   Directory to use to get file name
dataRE = re.compile ("data")
#   
spannerRE = re.compile (".*data.*")
#
format_preventRE = re.compile (".*format\_prevent")

class File (object) :
    '''   Structure to keep track of file size   '''
    __slots__ = 'size', 'file_name'
    def __init__ (self, size, file_name) :
        self.size = size
        self.file_name = file_name

class DirFile (object) :
    '''   Keep track of groups of files by size   '''
    __slots__ = ('size', 'files', 'name', 'group', 'base')
    def __init__ (self, sz, nm, base, group = None) :
        self.size = sz
        self.name = nm
        self.group = group
        self.base = base
        self.files = []
        
    def append (self, file) :
        self.files = self.files + file

class Files (Thread) :
    def __init__ (self, src_dir, dest_dir, sz, spanner = None) :
        Thread.__init__ (self)
        self.root = src_dir
        self.dest = dest_dir
        #   Current group
        self.current = 0
        sz = str (sz)
        #   kilobytes
        if sz[-1] == 'k' :
            self.size = int (float (sz[:-1])) * 1024
        #   megabytes
        elif sz[-1] == 'm' :
            self.size = int (float (sz[:-1])) * 1024 * 1024
        #   gigabytes
        elif sz[-1] == 'g' :
            self.size = int (float (sz[:-1])) * 1024 * 1024 * 1024
        #   bytes
        else :
            self.size = int (float (sz))
            
        #   Individual file and size
        self.all_files = []
        #   Group of files and size
        self.top = []
        #
        self.name = os.path.join ('YYYYJJJ', 'XX-XXXX')
        
        self.lock = Lock.lock ().LOCK

        #   Instance of Baler44_Span
        self.spanner = spanner
        
    def get_name (self, names) :
        '''   Make a YYYY_JJJ file name from YYYYMMDD   '''
        if not names : return
        k = {}
        s = []
        tdoy = TimeDoy.TimeDoy ()
        for n in names :
            t = n[-14:]
            k[t] = n
            s.append (t)
            
        s.sort ()
        n0 = k[s[0]]
        netsta, junk = n0.split ('_')
        yr, mo, da = yrmoda (s[0])
        doy = tdoy.doy (mo, da, yr)
        doyname = "%4d%03d" % (yr, doy)
        self.name = os.path.join (doyname, netsta)
            
    def read (self, nuke = True) :
        def build (arg, mydir, names) :
            if dataRE.match (mydir) :
                self.get_name (names)
                
            for n in names :
                f = os.path.join (mydir, n)
                if os.path.isdir (f) :
                    continue
                

                statinfo = os.stat (f)
                file_size = statinfo.st_size
                
                #   ***
                #   Need to generate start and end times file here
                #   ***
                #if spannerRE.match (f) and self.spanner :
                    #self.spanner.read (f)
                
                self.all_files.append (File (file_size, f))
        
        #try :
            #self.lock.acquire ()
        start_dir = os.getcwd ()
        os.chdir (self.root)
        BASEDIR = os.path.basename (self.root)
        dirs = os.listdir ('.')
        dirs.sort ()
        for d in dirs :
            #   Delete the format_prevent file
            if format_preventRE.match (d) and nuke :
                #now = time.strftime ("%Y%j%H%M", time.gmtime (time.time ()))
                #new_file = os.path.join (self.root, now)
                f = os.path.join (self.root, d)
                command = "rm -f %s" % f
                try :
                    #print command
                    os.system (command)
                except :
                    sys.stderr.write ("Error: Failed to rename %s!\n" % f)
                    
                continue
                
            if dirRE.match (d) :
                os.path.walk (d, build, self.root)
                    
        os.chdir (start_dir)
    #finally :
            #self.lock.release ()
                    
    def assign_group (self) :
        '''   Assign a group of files to create a total size   '''
        group = 0
        total = 0
        files = []
        for F in self.all_files :
            total += F.size
            #print total,
            if total > self.size :
                df = DirFile (total, self.name, self.root)
                df.append (files)
                df.group = group
                self.top.append (df)
                
                files = []
                group += 1
                total = F.size
            
            files.append (F.file_name)
                
        if group == 0 or files != [] :
            df = DirFile (total, self.name, self.root)
            df.append (files)
            df.group = group
            self.top.append (df)
            
        pass
                
    def rewind (self) :
        self.current = 0
         
    def get_next_group (self, what = None) :
        group = []
        if what == None :
            what = self.current
        
        for t in self.top :
            if t.group == what :
                group.append (t)
                
        if len (group) != 0 :
            self.current += 1
        else :
            self.rewind ()
            group = None
            
        return group
    
    def kill (self) :
        pass
    
    def run (self) :
        try :
            self.lock.acquire ()
            #print "Reading..."
            self.read ()
            #print "Groups..."
            self.assign_group ()
            #print "Spanner..."
            #self.spanner.write (os.path.join (self.dest, "time_spans.text"))
        finally :
            #print "Release..."
            self.lock.release ()
            
        sys.exit ()
    
###   Mixins   ###
def yrmoda (s) :
    yr = int (s[:4])
    mo = int (s[4:6])
    da = int (s[6:8])
    
    return yr, mo, da
    
if __name__ == "__main__" :
    import sys
    SIZE = 1024 * 1024 * 100   #   One hundred MB
    fl = Files ('/media/BALER44', str (SIZE))
    print "Reading...",
    fl.read ()
    print
    print "Assigning...",
    fl.assign_group ()
    print
    sys.exit ()
    while 1 :
        g = fl.get_next_group ()
        if g == None : break
        for ge in g :
            print ge.name, ge.group, ge.size
            for f in ge.files :
                print "\t", f