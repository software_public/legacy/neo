#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()


setup(
    author="IRIS PASSCAL",
    author_email='software-support@passcal.nmt.edu',
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved ::  GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 2.7',
    ],
    description="Copy data from RT-130 CF card to a zip file",
    entry_points={
        'console_scripts': [
            'neo=neo.main:main',
            'gents=neo.gents:main',
        ],
    },
    install_requires=['Pmw'],
    setup_requires = [],
    extras_require={
        'dev': [
            'pip',
            'bumpversion',
            'wheel',
            'watchdog',
            'flake8',
            'tox',
            'coverage',
            'Sphinx',
            'twine',
        ]
    },
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='neo',
    name='neo',
    packages=find_packages(include=['neo']),
    test_suite='tests',
    url='https://git.passcal.nmt.edu/passoft/neo',
    version='2018.179',
    zip_safe=False,
)
